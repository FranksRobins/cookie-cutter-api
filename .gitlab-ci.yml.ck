---
build-service:
  script:
    - echo $(pwd)
    - sudo docker build -t registry.gitlab.com/{{.Service.Repo.RepositoryOwner}}/{{.Service.Name}}:latest .
    - sudo docker push registry.gitlab.com/{{.Service.Repo.RepositoryOwner}}/{{.Service.Name}}:latest

deploy-nomad-service:
  needs:
    - job: build-service
  script:
    - terraform -chdir=$(pwd)/nomad init
    - terraform -chdir=$(pwd)/nomad apply -auto-approve -var jobspec_path="$(pwd)/nomad/jobspec/service.nomad"
